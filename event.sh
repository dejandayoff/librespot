#!/bin/sh

activity_file_path="/active"
touch $activity_file_path

# Non-blocking Events
if [ "$PLAYER_EVENT" == "changed" ]; then
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "playing"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"old_track_id\":\"$OLD_TRACK_ID\",\"track_id\":\"$TRACK_ID\"}"

elif [ "$PLAYER_EVENT" == "started" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "playing"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"track_id\":\"$TRACK_ID\"}"

elif [ "$PLAYER_EVENT" == "stopped" ]; then
    echo "not_active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "stopped"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"track_id\":\"$TRACK_ID\"}"

elif [ "$PLAYER_EVENT" == "playing" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "playing"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"track_id\":\"$TRACK_ID\",\"track_duration_ms\":\"$DURATION_MS\",\"track_position_ms\":\"$POSITION_MS\"}"

elif [ "$PLAYER_EVENT" == "paused" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "paused"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"track_id\":\"$TRACK_ID\",\"track_duration_ms\":\"$DURATION_MS\",\"track_position_ms\":\"$POSITION_MS\"}"

elif [ "$PLAYER_EVENT" == "preloading" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/status" -m "preloading"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/track" -m "{\"track_id\":\"$TRACK_ID\"}"

elif [ "$PLAYER_EVENT" == "volume_set" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/volume" -m "$VOLUME"

elif [ "$PLAYER_EVENT" == "volume_changed" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/volume" -m "$VOLUME"

elif [ "$PLAYER_EVENT" == "session_connected" ]; then
    echo "active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/session" -m "{\"connected\": true, \"username\":\"$USER_NAME\",  \"connection_id\":\"$CONNECTION_ID\", \"pa_server\": \"$(cat /pa_server)\"}"

elif [ "$PLAYER_EVENT" == "session_disconnected" ]; then
    echo "not_active" > /status
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/session" -m "{\"connected\": false, \"pa_server\": \"$(cat /pa_server)\"}"
fi