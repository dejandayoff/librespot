#!/bin/sh

# Issue
# When librespot crashes I need all of this to exit
# If this is playing and the pa server becomes available, then it restarts. I need to ignore if already connected.

GREEN='\033[0;32m'
NC='\033[0m' # No Color

activity_file_path="/active"
touch $activity_file_path

username=$SPOTIFY_USER
password=$SPOTIFY_PASS

topic="audio/available-audio-server"

command="/usr/bin/librespot --name \"$NAME\" --backend pulseaudio --mixer softvol --volume-range 1 --initial-volume $INIT_VOLUME --onevent /event.sh"

if [[ -n "$username" ]]; then
  echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Logging in with Username: $username"
  command="$command --username $username"
fi

if [[ -n "$password" ]]; then
  command="$command --password $password"
fi

if [[ "$(echo $VERBOSE | tr '[:upper:]' '[:lower:]')" == "true" ]]; then
  command="$command --verbose"
fi

echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Running Spotify with name: $NAME"

echo "not_active" > /status

echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Starting Avahi"

avahi-daemon -D

sleep 5

while true 
do
    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Obtaining Pulse Server"
    mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/connection" -m "active"
    mosquitto_sub -h $MQTT_HOST -t $topic --will-topic "audio/input/spotify/$NAME/connection" --will-payload "disconnected" | while read -r PAYLOAD
    do
        # echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Received Payload: $PAYLOAD"
        STATUS=$(cat /status)

        # Check if librespot has crashed.
        # This can be handled better because there will be a delay from the time of crash and exit.
        if [ -n "$pid" ] && ! kill -0 "$pid" > /dev/null 2>&1; then
            echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Librespot process with PID $pid is not running. It must have crashed... exiting."
            mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/connection" -m "disconnected"

            exit 1
        fi

        if [ "$STATUS" == "not_active" ]; then
            if [ "$PAYLOAD" != "None" ]; then
                if [ "$PAYLOAD" != "$PULSE_SERVER" ]; then
                    if [ -n "$pid" ]; then
                        echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Killing librespot on PID $pid."
                        kill "$pid" 2>/dev/null
                        unset pid
                    fi

                    export PULSE_SERVER=$PAYLOAD

                    echo $PULSE_SERVER > /pa_server

                    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Connecting to PulseAudio server at: $PULSE_SERVER."
                    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Initial volume set to: $INIT_VOLUME."

                    eval "$command &"
                    pid=$!

                    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Librespot running with PID: $pid."
                fi
            else
                if [ -z ${pid+x} ]; then 
                    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   No available PulseAudio servers. librespot did not start."
                fi
            fi
        else
            echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Spotify is active, not changing anything."
        fi
    done

    if [[ -f "$activity_file_path" ]]; then
        last_modified=$(stat -c %Y "$activity_file_path") # Get the last modification time of the file in seconds since epoch
        current_time=$(date +%s) # Get the current time in seconds since epoch
        time_difference=$((current_time - last_modified)) # Calculate the time difference in seconds
        
        if [[ $time_difference -gt 3600 ]]; then # 3600 seconds = 1 hour
            echo "File hasn't been modified in over 1 hour. Exiting."
            mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/connection" -m "disconnected"
            exit 0
        fi
    else
        echo "File not found. Exiting."
        mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/connection" -m "disconnected"
        exit 1
    fi

    sleep 10
done

mosquitto_pub -h $MQTT_HOST -t "audio/input/spotify/$NAME/connection" -m "disconnected"
