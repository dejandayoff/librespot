
# Build process from: https://git.alpinelinux.org/aports/tree/testing/librespot/APKBUILD
FROM alpine:3.18 as librespot-builder
WORKDIR /app

RUN apk add pulseaudio-dev cargo curl avahi-compat-libdns_sd pkgconfig avahi-dev alsa-lib-dev 

ARG LIBRESPOT_VERSION=dev


RUN curl -sL "https://github.com/librespot-org/librespot/archive/${LIBRESPOT_VERSION}.tar.gz" --output librespot.tar.gz && \
  mkdir /app/librespot-src && \
  tar -zxvf librespot.tar.gz --directory /app/librespot-src --strip-components=1

WORKDIR /app/librespot-src

ENV CARGO_HOME=/app/cargo
ENV RUSTFLAGS="-C target-feature=-crt-static"

RUN cargo build \
  --release \
  --features with-dns-sd,pulseaudio-backend \
  --verbose

FROM alpine:3.18
WORKDIR /app

RUN apk add libgcc pulseaudio-dev mosquitto-clients avahi-compat-libdns_sd alsa-lib-dev avahi dumb-init

RUN sed -i 's/#enable-dbus=yes/enable-dbus=no/g' /etc/avahi/avahi-daemon.conf

COPY --from=librespot-builder /app/librespot-src/target/release/librespot /usr/bin/librespot

COPY ./entrypoint.sh /entrypoint.sh
COPY ./event.sh /event.sh

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD [ "/entrypoint.sh" ]